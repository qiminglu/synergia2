collective module
==================
The collective module needs to be described.

---------
Classes
---------

.. doxygenclass:: Rectangular_grid
   :project: synergia

.. doxygenclass:: Rectangular_grid_domain
    :project: synergia

.. doxygenclass:: Distributed_rectangular_grid
    :project: synergia

.. doxygenclass:: Space_charge_3d_open_hockney
    :project: synergia
