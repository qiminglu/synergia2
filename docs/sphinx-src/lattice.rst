lattice module
==================
The lattice module contains lattice elements, lattices and related classes.

---------
Classes
---------

.. doxygenclass:: Lattice
   :project: synergia

.. doxygenclass:: Lattice_element
   :project: synergia

.. doxygenclass:: Lattice_element_slice
   :project: synergia

.. doxygenclass:: Chef_lattice
   :project: synergia

.. doxygenclass:: Element_adaptor
   :project: synergia

.. doxygenclass:: Element_adaptor_map
   :project: synergia

.. doxygenclass:: Marker_mad8_adaptor
   :project: synergia

.. doxygenclass:: Drift_mad8_adaptor
   :project: synergia

.. doxygenclass:: Sbend_mad8_adaptor
   :project: synergia

.. doxygenclass:: Rbend_mad8_adaptor
   :project: synergia

.. doxygenclass:: Quadrupole_mad8_adaptor
   :project: synergia

.. doxygenclass:: Sextupole_mad8_adaptor
   :project: synergia

.. doxygenclass:: Octupole_mad8_adaptor
   :project: synergia

.. doxygenclass:: Multipole_mad8_adaptor
   :project: synergia

.. doxygenclass:: Solenoid_mad8_adaptor
   :project: synergia

.. doxygenclass:: Hkicker_mad8_adaptor
   :project: synergia

.. doxygenclass:: Vkicker_mad8_adaptor
   :project: synergia

.. doxygenclass:: Kicker_mad8_adaptor
   :project: synergia

.. doxygenclass:: Rfcavity_mad8_adaptor
   :project: synergia

.. doxygenclass:: Elseparator_mad8_adaptor
   :project: synergia

.. doxygenclass:: Hmonitor_mad8_adaptor
   :project: synergia

.. doxygenclass:: Vmonitor_mad8_adaptor
   :project: synergia

.. doxygenclass:: Monitor_mad8_adaptor
   :project: synergia

.. doxygenclass:: Instrument_mad8_adaptor
   :project: synergia

.. doxygenclass:: Ecollimator_mad8_adaptor
   :project: synergia

.. doxygenclass:: Rcollimator_mad8_adaptor
   :project: synergia


---------
Typedefs
---------

.. doxygentypedef:: Chef_elements
    :project: synergia

.. doxygentypedef:: Chef_lattice_sptr
    :project: synergia

.. doxygentypedef:: Element_adaptor_sptr
    :project: synergia

.. doxygentypedef:: Element_adaptor_map_sptr
    :project: synergia

.. doxygentypedef:: Lattice_element_sptr
    :project: synergia

.. doxygentypedef:: Lattice_elements
    :project: synergia

.. doxygentypedef:: Lattice_element_slice_sptr
    :project: synergia

.. doxygentypedef:: Lattice_element_slices
    :project: synergia

.. doxygentypedef:: Lattice_sptr
    :project: synergia

