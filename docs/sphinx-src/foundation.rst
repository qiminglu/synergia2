foundation module
==================
The foundation module contains classes fundamental to accelerator physics and
accelerator physics simulation.

---------
Classes
---------

.. doxygenclass:: Four_momentum
   :project: synergia

.. doxygenclass:: Reference_particle
    :project: synergia

.. doxygenclass:: Distribution
    :project: synergia

.. doxygenclass:: Random_distribution
    :project: synergia
