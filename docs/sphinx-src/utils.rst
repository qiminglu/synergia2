utils module
==================
The utils module contains utilities of general use, i.e., not specific to Synergia or
even accelerator physics.

---------
Classes
---------

.. doxygenclass:: Commxx
  	:project: synergia

.. doxygenclass:: Distributed_fft3d
	:project: synergia

.. doxygenclass:: Hdf5_writer
	:project: synergia

.. doxygenclass:: Hdf5_serial_writer
	:project: synergia

.. doxygenclass:: Hdf5_chunked_array2d_writer
	:project: synergia

---------
Typedefs
---------

.. doxygentypedef:: MArray1d
    :project: synergia

.. doxygentypedef:: MArray1d_ref
    :project: synergia

.. doxygentypedef:: Const_MArray1d_ref
    :project: synergia

.. doxygentypedef:: MArray2d
    :project: synergia

.. doxygentypedef:: MArray2d_ref
    :project: synergia

.. doxygentypedef:: Const_MArray2d_ref
    :project: synergia


