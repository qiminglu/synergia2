#ifndef SYNERGIA_H_
#define SYNERGIA_H_

int
synergia_major_version();

int
synergia_minor_version();

int
synergia_subminor_version();

#endif /* SYNERGIA_H_ */
